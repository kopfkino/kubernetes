#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OLDDIR=$PWD
cd $DIR
kubectl delete -n kopfkino -f mongodb.yml -f agendash.yml -f rethinkdb.yml
kubectl delete -n kopfkino -f https://gitlab.com/kopfkino/sync/raw/master/sync.yml
kubectl delete -n kopfkino -f https://gitlab.com/kopfkino/api/raw/master/api.yml
kubectl delete -n kopfkino -f https://gitlab.com/kopfkino/ui/raw/master/ui.yml
cd $OLDDIR