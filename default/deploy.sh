#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OLDDIR=$PWD
echo $DIR
cd $DIR
kubectl create clusterrolebinding permissive-binding \
  --clusterrole=cluster-admin \
  --user=admin \
  --user=kubelet \
  --group=system:serviceaccounts
kubectl create ns nginx-ingress
kubectl apply -f default-rbac.yml 
kubectl apply -f helm-rbac.yml
helm init --wait
helm install --name nginx-ingress -f nginx-ingress.yml --namespace nginx-ingress stable/nginx-ingress
helm install --name cert-manager --namespace kube-system stable/cert-manager \
  --set ingressShim.extraArgs='{--default-issuer-name=letsencrypt-production,--default-issuer-kind=ClusterIssuer}'
kubectl apply -f cluster-issuer.yml
cd $OLDDIR
