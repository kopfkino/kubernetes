#!/bin/bash
source env.sh
gcloud config set project $PROJECT_ID
gcloud config set compute/zone $COMPUTE_ZONE
gcloud config set container/use_v1_api false
gcloud beta container clusters delete $CLUSTER_NAME