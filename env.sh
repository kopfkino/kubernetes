#!/bin/bash
export PROJECT_ID=kopfkino-200721
export COMPUTE_ZONE=europe-west3
export CLUSTER_NAME=kopfkino
export MACHINE_TYPE=g1-small
export NUM_NODES=2
export DOMAIN=barath.me
