#!/bin/bash
source env.sh
gcloud config set project $PROJECT_ID
gcloud config set compute/zone $COMPUTE_ZONE
gcloud config set container/use_v1_api false
gcloud beta container clusters create $CLUSTER_NAME --cluster-version 1.9.6-gke.0 --machine-type $MACHINE_TYPE --num-nodes $NUM_NODES
gcloud beta container clusters get-credentials $CLUSTER_NAME
source default/deploy.sh
ingress_ip=$(kubectl get svc -n nginx-ingress -o json | jq '.items[].status.loadBalancer.ingress[0].ip | select(. != null) ' -r)
while (( $ingress_ip == null ))
do
  echo Waiting for External IP...
  ingress_ip=$(kubectl get service nginx-ingress-controller -n nginx-ingress -o json | jq .status.loadBalancer.ingress[0].ip -r)
  sleep 2
done
echo External IP is $ingress_ip
source kopfkino/deploy.sh
rm ./transaction.yaml
# gcloud dns managed-zones create --dns-name="$DOMAIN" --description="$DOMAIN zone" ${PROJECT_ID} || true
gcloud dns record-sets transaction start -z=${PROJECT_ID}
gcloud dns record-sets transaction add -z=${PROJECT_ID} --name="$DOMAIN" --type=A --ttl=300 $ingress_ip || true
gcloud dns record-sets transaction add -z=${PROJECT_ID} --name="*.$DOMAIN" --type=A --ttl=300 $ingress_ip || true
gcloud dns record-sets transaction execute -z=${PROJECT_ID}