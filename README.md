# Introduction
**Kopfkino** is a simple application that aggregates **brickblock** ICO transaction data with enhanced data from the blockchain and historical exchange information to provide quick visualizations of the contributions to **brickblock**.

The APIs used are as follow:

  * Input API from https://blockchain.brickblock.io/inputs
  * Transaction data from https://www.blockcypher.com
  * Historical transaction information from https://www.cryptocompare.com

# Application Architecture
The application itself consists of 3 components:

## [Sync](https://gitlab.com/kopfkino/sync)
  1. Looks for new transactions in the input API (every 5 minutes by default)
  2. Processes those transactions with historical exchange rates and enhanced transaction data from the blockchain
  3. Populates the data in a database for easy retrieval later through the API

## [API](https://gitlab.com/kopfkino/api)

  The API is GraphQL based and provides an interface to retrieve arbitrary fields from the database
## [UI](https://gitlab.com/kopfkino/ui)

  The front end of the application that allows the user to create visualizations based on the data in the database
## [Kubernetes](https://gitlab.com/kopfkino/kubernetes)
  Used for orchestrating the above services and their dependencies
## Supporting Services
  * MongoDB: Keeps some data about the `sync` schedule through the [agenda](https://www.npmjs.com/package/agenda) library form node
  * RethinkDB: Keeps transaction data from the `sync` application
  * Agendash: A front end for `agenda`

# Application Usage
You can connect to any of the following URLs to use the application
  * UI - https://barath.me
  * API - https://api.barath.me
  * Sync schedule - https://sync.barath.me

# Application Deployment
This repository deploys a Kubernetes cluster on Google Cloud and points 2 domain names to the cluster.
The application is then deployed on the Kubernetes cluster

## Details
### Running the Deployment
To deploy the cluster and the application:

1. Edit the file `env.sh` and modify the contents to suit the purposes of the implementation
2. Ensure that you are logged into Google Cloud with the gcloud client
3. Run `./cluster.sh`

### Explanation
Ref: `cluster.sh`
* The cluster is brought up in (lines 2-7)
* The applications are deployed (lines 8-9)
* Wait for the ingress controller to receive an external ip (lines 12-17)
* Set up managed DNS zones on Google Cloud (lines 19-20)
* Set up record sets for the domain and wildcard subdomains (lines 21-28)

### CI/CD

* The above applications will automatically publish to Dockerhub on every commit and also deploy to the Kubernetes cluster. The `.gitlab-ci.yml` files can be found in the respective application repositories
* The Kubernetes cluster can also be deployed automatically, but has been set to manual. As a future improvement, the cluster may be built using a solution that manages state (like `terraform`). This will ensure that any changes to the cluster will be version controlled and the CI process will be much smoother. At the current time, the cluster will be destroyed and recreated when the CI process is manually initiated.
* The application deployments during the cluster provisioning is directly from the raw `yaml` files from the Gitlab repository.

### Applications

* Default Kubernetes setup
  * Set up an admin cluster role binding
  * Set up default RBAC for the default service account
  * Set up RBAC for Helm
  * Set up a cluster SSL certificate issuer for LetsEncrypt
  * Install Helm
  * Install the Nginx Ingress Controller
  * Install `cert-manager` to handle automated certificate issuing
* Application setup
  * Create a namespace named `kopfkino`
  * Deploy the supporting services and applications
    * MongoDB
    * RethinkDB
    * Agendash
    * Sync
    * API
    * UI

# Caveats
  * The deployment expects a managed zone with the required domain name ready for use. LetsEncrypt performs an http01 challenge and expects to find a particular file at a particular URL on the domain. As DNS propogation can take some time, a previously prepared managed zone is required for SSL certificate requests to be successful.
  * This is not a production level cluster. As such, some permissions in the Kubernetes RBAC are more permissive than they should be
  * The free tier of the APIs used to enhance transaction information have pretty tight rate limits. As such, the sync process may take a few attempts to fully synchronize. The sync process in incremental, so any subsequent calls to the API will be limited to new entries. As such, the free tier limit should be sufficient to handle subsequent loads.